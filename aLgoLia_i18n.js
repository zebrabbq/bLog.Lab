var atomicalgolia = require("atomic-algolia")

var indexes = [
    {
        name: "zbbq.en",
        path: "public/index.json"
    },
    {
        name: "zbbq.ko",
        path: "public/ko/index.json"
    }
]
var cb = function (error, result) {
    if (error) throw error

    console.log(result)
}

indexes.forEach(function(index) {
  atomicalgolia(index.name, index.path, cb);
});